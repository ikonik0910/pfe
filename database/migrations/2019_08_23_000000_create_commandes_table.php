<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('shipper_id')->unsigned();
            $table->bigInteger('payment_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->string('quantity')->unique();
            $table->float('price')->nullable()->default('0');
            $table->float('total')->nullable()->default('0');
            $table->string('color')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            // Foreing keys
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('shipper_id')->references('id')->on('shippers');
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('customer_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
