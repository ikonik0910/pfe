<?php

namespace Database\Seeders;

use App\Models\Commande;
use Illuminate\Database\Seeder;

class CommandesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commandes = [
            [
                'product_id'        => '1',
                'shipper_id'           => '1',
                'payment_id'           => '1',
                'customer_id'           => '2',
                'quantity'           => '3',
                'price'           => '350',
                'total'           => '1050',
                'color'           => 'green',
                'status'           => 'livred',

            ],
            [
                'product_id'        => '6',
                'shipper_id'           => '1',
                'payment_id'           => '2',
                'customer_id'           => '2',
                'quantity'           => '10',
                'price'           => '250',
                'total'           => '2500',
                'color'           => 'red',
                'status'           => 'stocked',

            ],
            [
                'product_id'        => '1',
                'shipper_id'           => '2',
                'payment_id'           => '1',
                'customer_id'           => '4',
                'quantity'           => '1',
                'price'           => '350',
                'total'           => '350',
                'color'           => 'green',
                'status'           => 'livred',

            ],
            [
                'product_id'        => '8',
                'shipper_id'           => '3',
                'payment_id'           => '2',
                'customer_id'           => '7',
                'quantity'           => '2',
                'price'           => '450',
                'total'           => '900',
                'color'           => 'white',
                'status'           => 'livred',

            ],
        ];
        Commande::insert($commandes);
    }
}
