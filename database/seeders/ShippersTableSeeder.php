<?php

namespace Database\Seeders;

use App\Models\Shipper;
use Illuminate\Database\Seeder;

class ShippersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shippers = [
            [
                'shipper_name'        => 'Amana',
                'city'           => 'Agadir',
                'phone'           => '0565765432',
                'address'           => 'Hay Sabt Agadir',
            ],
            [
                'shipper_name'        => 'Cash',
                'city'           => 'Guelmim',
                'phone'           => '0565111132',
                'address'           => 'Hay Molay Ahmed Guelmim',
            ],
            [
                'shipper_name'        => 'Delevry Amine',
                'city'           => 'Casa',
                'phone'           => '0565111100',
                'address'           => 'Hay Molay Saltan Casa',
            ],
        ];
        Shipper::insert($shippers);
    }
}
