<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'role_id'        => '1',
                'user_name'           => 'admin123',
                'first_name'           => 'Admin',
                'last_name'           => 'Admin',
                'sex'           => 'Male',
                'phone'           => '0600000000',
                'address'           => 'Agadir',
                'email'          => 'admin123@admin.com',
                'password'       => bcrypt('admin123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/50.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'yassin123',
                'first_name'           => 'Yassin',
                'last_name'           => 'Yassin',
                'sex'           => 'Male',
                'phone'           => '0627292827',
                'address'           => 'Casa',
                'email'          => 'yassin123@customer.com',
                'password'       => bcrypt('yassin123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/51.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'mohamed123',
                'first_name'           => 'Mohamed',
                'last_name'           => 'Mohamed',
                'sex'           => 'Male',
                'phone'           => '0600004444',
                'address'           => 'Fès',
                'email'          => 'mohamed123@customer.com',
                'password'       => bcrypt('mohamed123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/52.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'amina123',
                'first_name'           => 'Amina',
                'last_name'           => 'Amina',
                'sex'           => 'female',
                'phone'           => '0604567800',
                'address'           => 'Agadir',
                'email'          => 'amina123@customer.com',
                'password'       => bcrypt('amina123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/53.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'imane123',
                'first_name'           => 'Imane',
                'last_name'           => 'Imane',
                'sex'           => 'Female',
                'phone'           => '0645000000',
                'address'           => 'Agadir',
                'email'          => 'imane123@customer.com',
                'password'       => bcrypt('imane123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/54.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'fatima123',
                'first_name'           => 'Fatima',
                'last_name'           => 'Fatima',
                'sex'           => 'Female',
                'phone'           => '0600345300',
                'address'           => 'Agadir',
                'email'          => 'fatima123@customer.com',
                'password'       => bcrypt('fatima123'),
                'avatar' => 'https://randomuser.me/api/portraits/men/55.jpg',
                'remember_token' => Str::random(60),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'oumaima123',
                'first_name'           => 'Oumaima',
                'last_name'           => 'Oumaima',
                'sex'           => 'Female',
                'phone'           => '0623345300',
                'address'           => 'Agadir',
                'email'          => 'oumaima123@customer.com',
                'password'       => bcrypt('oumaima123'),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'ali123',
                'first_name'           => 'Ali',
                'last_name'           => 'Ali',
                'sex'           => 'Male',
                'phone'           => '0611115300',
                'address'           => 'Guelmim',
                'email'          => 'ali123@customer.com',
                'password'       => bcrypt('ali123'),
            ],
            [
                'role_id'        => '2',
                'user_name'           => 'najib123',
                'first_name'           => 'Najib',
                'last_name'           => 'Najib',
                'sex'           => 'Male',
                'phone'           => '0600111109',
                'address'           => 'Tanger',
                'email'          => 'najib123@customer.com',
                'password'       => bcrypt('najib123'),
            ],
            [
                'role_id'        => '3',
                'user_name'           => 'ahmed123',
                'first_name'           => 'Ahmed',
                'last_name'           => 'Ahmed',
                'sex'           => 'Male',
                'phone'           => '0645677897',
                'address'           => 'Tanger',
                'email'          => 'ahmed123@user.com',
                'password'       => bcrypt('ahmed123'),
            ],
            [
                'role_id'        => '3',
                'user_name'           => 'youssef123',
                'first_name'           => 'Youssef',
                'last_name'           => 'Youssef',
                'sex'           => 'Male',
                'phone'           => '0645123497',
                'address'           => 'Casa',
                'email'          => 'youssef123@user.com',
                'password'       => bcrypt('youssef123'),
            ], [
                'role_id'        => '3',
                'user_name'           => 'hind123',
                'first_name'           => 'Hind',
                'last_name'           => 'Hind',
                'sex'           => 'Female',
                'phone'           => '0611277897',
                'address'           => 'Tanger',
                'email'          => 'hind123@user.com',
                'password'       => bcrypt('hind123'),
            ],
        ];
        User::insert($users);
    }
}
