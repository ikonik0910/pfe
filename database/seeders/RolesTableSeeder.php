<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'        => 'admin',
                'display_name'           => 'Administrator',
            ],
            [
                'name'        => 'customer',
                'display_name'           => 'Customer',
            ],
            [
                'name'        => 'user',
                'display_name'           => 'Normal User',
            ],
        ];
        Role::insert($roles);
    }
}
