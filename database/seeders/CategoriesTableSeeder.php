<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'order'        => '1',
                'name'           => 'Farm Products',
                'slug'           => 'farm_products',
            ],
            [
                'order'        => '1',
                'name'           => 'Natural Products',
                'slug'           => 'natural_products',
            ],
            [
                'order'        => '1',
                'name'           => 'Manufactured Products',
                'slug'           => 'manufactured_products',
            ],
            [
                'order'        => '1',
                'name'           => 'Supplies',
                'slug'           => 'supplies',
            ],
        ];
        Category::insert($categories);
    }
}
