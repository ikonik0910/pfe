<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'category_id'        => '1',
                'product_name'           => 'Burlap Bunny Lemon Cypress Tree',
                'stocks'           => '80',
                'price'           => '350',
                'image'           => 'https://contenthandler.azureedge.net/prod/3121/2/0/0/coastal-nursery_burlap-bunny-lemon-cypress-blue.jpg',
                'description'           => 'Established in 1946, Coastal Nursery grows select plants that flourish in the cool nights, sunny days and coastal breezes that make up their moderate Mediterranean climate near Monterey, California. With operations across seven farms and more than 140 acres, their specialties include several varieties of cypress and the rare ventricosa heather. Festive arrangements make these unique plants fun to gift and grow throughout the year.',

            ],
            [
                'category_id'        => '2',
                'product_name'           => 'Amlou Pistache 190g',
                'stocks'           => '22',
                'price'           => '60',
                'image'           => 'https://www.terrebrune.ma/wp-content/uploads/2019/04/2-TB-AMLOU-pistachio-600x505.jpg',
                'description'           => 'Amlou pistache est la pâte à tartiner marocaine d’origine berbère au goût pistache. Elle est produite à base d’amandes, d’huile d’argan, de miels et de pistache. ',

            ],
            [
                'category_id'        => '4',
                'product_name'           => 'Silver hand-made lamp',
                'stocks'           => '10',
                'price'           => '8',
                'image'           => 'https://i.ebayimg.com/images/g/1qQAAOSwVJhZPx3B/s-l300.jpg',
                'description'           => 'A beautiful old Silver Teapot lamp and two soy candles with wood wicks. A wonderful dream lamp! Set may vary slightly from image. 14 inches in height. Takes a standard bulb (not included due to shipping'

            ],
            [
                'category_id'        => '1',
                'product_name'           => 'LAVENDER ENCHANTMENT BOUQUET',
                'stocks'           => '30',
                'price'           => '59',
                'image'           => 'https://contenthandler.azureedge.net/prod/3103/1/0/0/enchantmentfar.jpg',
                'description'           => 'Sun Vista Farms creates beautiful handmade bouquets using locally-sourced blooms from their own farm and neighboring farms in North County San Diego.Views from their Pauma Valley farm are stunning as rows of avocado orchards roll into fields of fragrant waxflowers and numerous varieties of gorgeously colored and textured greenery. Sun Vista\'s emphasis on natural textures and absolute freshness brings bouquets that capture the color, scent and feel of the season.',

            ],[
                'category_id'        => '1',
                'product_name'           => 'SUPER-FINE ALMOND FLOUR',
                'stocks'           => '150',
                'price'           => '120',
                'image'           => 'https://contenthandler.azureedge.net/prod/2650/1/0/0/Almond-flour.jpg',
                'description'           => 'For over three decades, Bob has been committed to providing people everywhere with the best quality foods available. His passion for health and belief in taking care of one another is as strong today as it was when he first started the business with his wife, Charlee, all those years ago. It\'s this passion for providing the very best nutritional whole grains that has helped make Bob\'s Red Mill a leading name in health foods across the country.',

            ],[
                'category_id'        => '2',
                'product_name'           => 'Organic Argan oil',
                'stocks'           => '20',
                'price'           => '250',
                'image'           => 'https://images-na.ssl-images-amazon.com/images/I/61%2BGVPQwMfL._AC_UL160_.jpg',
                'description'           => 'Common toxins such as pollution or sun exposure can cause further damage to skin and hair. Antioxidants from Argan Oil adds an extra layer of protection against environmental factors.',

            ],
            [
                'category_id'        => '2',
                'product_name'           => 'Olive Oil 500ml',
                'stocks'           => '23',
                'price'           => '100',
                'image'           => 'https://images-eu.ssl-images-amazon.com/images/I/71BnH1dzKIL._AC_UL160_.jpg',
                'description'           => 'Ideal for Cold Cooking – The superior flavour and oil comes from the first pressing of the finest olives. Turn your boring healthy meals into a mouth-watering affair with salad dressings, dips, marinades and chutneys prepared in the goodness of Figaro extra virgin olive oil',

            ],
            [
                'category_id'        => '4',
                'product_name'           => 'Moroccan Berber Tribal Silver Cuff',
                'stocks'           => '130',
                'price'           => '450',
                'image'           => 'https://cdn.shopify.com/s/files/1/2802/0948/products/1_main-10_l_600x.jpg?v=1525256913',
                'description'           => 'Moroccan Berber tribal bracelet Moroccan tribal bracelet from the High Atlas of Morocco. Handcrafted by Berber women using Moroccan silver nickel. The ethnic Nomadic and Bedouin jewelry from the Maghreb and North Africa is usually made of silver and the designs are full of symbolism to protect from the evil eye. This bracelet is a stunning piece of Tribal jewelry. Hinged closure. Measures: Inside 2.5" in diameter, outside 3" in diameter.',

            ],
            [
                'category_id'        => '1',
                'product_name'           => 'BUNCHED BELLS OF IRELAND',
                'stocks'           => '',
                'price'           => '',
                'image'           => 'https://contenthandler.azureedge.net/prod/2889/1/0/0/bells.jpg',
                'description'           => 'Sun Vista Farms creates beautiful handmade bouquets using locally-sourced blooms from their own farm and neighboring farms in North County San Diego. Views from their Pauma Valley farm are stunning as rows of avocado orchards roll into fields of fragrant waxflowers and numerous varieties of gorgeously colored and textured greenery. Sun Vista\'s emphasis on natural textures and absolute freshness brings bouquets that capture the color, scent and feel of the season.',

            ],
        ];
        Product::insert($products);
    }
}
