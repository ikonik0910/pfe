<?php

namespace Database\Seeders;

use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payments = [
            [
                'payment_type'        => 'Cash On delevry',
            ],
            [
                'payment_type'        => 'Paypal',
            ],
            [
                'payment_type'        => 'Mester card',
            ],
            
        ];
        Payment::insert($payments);
    }
}
